package com.mev.task.service.impl;

import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

public class SelectServiceImplTest {

    @Autowired
    private MongoTemplate template;


    public SelectServiceImplTest() throws JSONException {
        String jsonString = "{selectFields:['*'],from:'cars'}";
        JSONObject jsonObject = new JSONObject(jsonString);


    }

    @Test
    public void selectLike() {

    }

    @Test
    public void conditionToDocAddToArgList() throws Exception {
        String andOrOperator = "and";
        List<Bson> listArg = new ArrayList<>();
        List<String> andCondition = Arrays.asList("a>b", "b<c");
        String where = "a=w";
        List<Bson> doc = new ArrayList<>();
        for (String el : andCondition) {
            doc.add(conditionConverToFiletr(el));
        }
        doc.add(conditionConverToFiletr(where));

        if (andOrOperator.equals("and"))
            listArg.add(Aggregates.match(Filters.and(doc)));
        else if (andOrOperator.equals("or"))
            listArg.add(Aggregates.match(Filters.or(doc)));

        Assert.assertEquals(listArg.size(), 1);
    }


    private Bson conditionConverToFiletr(String el) throws Exception {
        if (el.length() > 0)
            return Filters.eq("a", "b");
        else
            throw new Exception();
    }

    @Test
    public void convertResultToOut() {
        List<String> inListResult = new ArrayList<>();
        inListResult.add("{ '_id' : { '$oid' : '5b44f555e523821e184b1b38' }, 'name' : 'camry', 'brand' : 'toyota' }");
        JSONArray select = new JSONArray();
        select.put("*");
        List<String> outList = new ArrayList<>();
        inListResult.forEach(el -> {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(el);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        jsonObject.remove("_id");
                        Iterator<String> keys = jsonObject.keys();
                        String out = "";
                        while (keys.hasNext()) {
                            String key = keys.next();
                            out += (jsonObject.get(key)).toString() + " ";
                        }
                        outList.add(out);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


        );

        Assert.assertEquals(outList.size(),1);
    }

    @Test
    public void addEmployee() {
    }

    @Test
    public void addCar() {
    }
}