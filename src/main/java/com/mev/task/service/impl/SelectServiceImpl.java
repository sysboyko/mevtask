package com.mev.task.service.impl;
import com.mev.task.MyException;
import com.mev.task.service.SelectService;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class SelectServiceImpl
        implements SelectService {
    @Autowired
    private MongoTemplate template;



    private final static List<String> operatorsList = Arrays.asList("=", ">", ">=", "<", "<=", "<>");
    private final static Map<String, String> operatorsConvert;

    static {
        operatorsConvert = new HashMap<String, String>();
        operatorsConvert.put("=", "$eq");
        operatorsConvert.put(">", "$gt");
        operatorsConvert.put(">=", "$gte");
        operatorsConvert.put("<", "$lt");
        operatorsConvert.put("<=", "$lte");
        operatorsConvert.put("<>", "$lte");
    }

    @Override
    public List<String> selectLike(JSONObject json) throws MyException {
        System.out.println(json);
        if (json.has("selectFields")) {
            if (json.getJSONArray("selectFields").length() == 0) {
                throw new MyException();
            }
        } else {
            throw new MyException();
        }


        List<Bson> listArg = new ArrayList<>();
        //FROM
        MongoCollection<Document> collection = template.getCollection(json.getString("from"));

        List<String> result = new ArrayList<>();
        Block<Document> setResult = document -> {
            result.add(document.toJson());
//            System.out.println(document.toJson());
        };

        //WHERE WITH AND OR
        if (json.has("andOrObj")) {
            JSONArray array = json.getJSONArray("andOrObj");
            List<Bson> orList = new ArrayList<>();
            array.forEach(el -> {
                        if (el instanceof JSONArray) {
                            List<Bson> listAndEmb = new ArrayList<>();
                            ((JSONArray) el).forEach(e -> listAndEmb.add(conditionConverToFiletr((String) e)));
                            orList.add(Filters.and(listAndEmb));
                        } else {
                            orList.add(conditionConverToFiletr((String) el));
                        }
                    }
            );
            listArg.add(Aggregates.match(Filters.or(orList)));
        } else {
            if (json.has("and") || json.has("or")) {
                if (json.has("and")) {
                    JSONArray andArr = json.getJSONArray("and");
                    List<String> andCondition = new ArrayList<>();
                    for (int i = 0; i < andArr.length(); i++) {
                        andCondition.add(andArr.get(i).toString());
                    }
                    conditionToDocAddToArgList(listArg, "and", andCondition, json.getString("where"));
                }
                if (json.has("or")) {
                    JSONArray andArr = json.getJSONArray("or");
                    List<String> orCondition = new ArrayList<>();
                    for (int i = 0; i < andArr.length(); i++) {
                        orCondition.add(andArr.get(i).toString());
                    }
                    conditionToDocAddToArgList(listArg, "or", orCondition, json.getString("where"));
                }
            } else {
                if (json.has("where")) {
                    listArg.add(Aggregates.match(conditionConverToFiletr(json.getString("where").trim())));
                }
            }
        }
        if (json.has("orderBy")) {
            List<Document> manyFields = new ArrayList<>();
            int sorting;
            if (json.getString("sort").trim().equals("asc")) {
                sorting = 1;
            } else {
                sorting = -1;
            }
            if (json.get("orderBy") instanceof JSONArray) {
                json.getJSONArray("orderBy").forEach(el -> manyFields.add(new Document(el.toString(), sorting)));
                listArg.add(new Document("$sort", manyFields));
            } else {
                listArg.add(new Document("$sort", new Document(json.getString("orderBy").trim(), sorting)));
            }
        }

        //SKIP
        if (json.has("skip")) {
            if (json.getInt("skip") > 0) {
                listArg.add(new Document("$skip", json.getInt("skip")));
            }
        }
        //limit
        if (json.has("limit")) {
            if (json.getInt("limit") > 0) {
                listArg.add(new Document("$limit", json.getInt("limit")));
            }
        }


        System.err.println(listArg.toString());
        if (listArg.size() > 0) {
            collection.aggregate(listArg).forEach(setResult);
            return convertResultToOut(result, json.getJSONArray("selectFields"));
        } else {
            collection.find().forEach(setResult);
            return convertResultToOut(result, json.getJSONArray("selectFields"));
        }

    }


    private Bson conditionConverToFiletr(String where) {
        String operator = "";
        String field = "";
        String val = "";
        for (String op : operatorsList) {
            if (where.contains(op)) {
                operator = op;
                field = where.split(op)[0];
                val = where.split(op)[1];
            }
        }
        if (val.matches("-?\\d+(\\.\\d+)?")) {
            switch (operator) {
                case "=":
                    return Filters.eq(field, Integer.parseInt(val));
                case ">":
                    return Filters.gt(field, Integer.parseInt(val));
                case ">=":
                    return Filters.gte(field, Integer.parseInt(val));
                case "<":
                    return Filters.lt(field, Integer.parseInt(val));
                case "<=":
                    return Filters.lte(field, Integer.parseInt(val));
                case "<>":
                    return Filters.ne(field, Integer.parseInt(val));
                default:
                    return null;
            }
        } else {
            switch (operator) {
                case "=":
                    return Filters.eq(field, val);
                case ">":
                    return Filters.gt(field, val);
                case ">=":
                    return Filters.gte(field, val);
                case "<":
                    return Filters.lt(field, val);
                case "<=":
                    return Filters.lte(field, val);
                case "<>":
                    return Filters.ne(field, val);
                default:
                    return null;
            }
        }
    }

    void conditionToDocAddToArgList(List<Bson> listArg, String andOrOperator, List<String> andCondition, String where) {
        List<Bson> doc = new ArrayList<>();
        for (String el : andCondition) {
            doc.add(conditionConverToFiletr(el));
        }
        doc.add(conditionConverToFiletr(where));

        if (andOrOperator.equals("and"))
            listArg.add(Aggregates.match(Filters.and(doc)));
        else if(andOrOperator.equals("or"))
            listArg.add(Aggregates.match(Filters.or(doc)));
    }


    private List<String> convertResultToOut(List<String> inListResult, JSONArray select) {
        List<String> outList = new ArrayList<>();
        System.out.println("Select size " + select.length());
        System.out.println("inListResult size " + inListResult.size());
        System.out.println(inListResult);



        inListResult.forEach(el -> {
                    JSONObject jsonObject = new JSONObject(el);
                    if (select.get(0).equals("*")) {
                        jsonObject.remove("_id");
                        Iterator<String> keys = jsonObject.keys();
                        String out = "";
                        while (keys.hasNext()) {
                            String key = keys.next();
                            out += (jsonObject.get(key)).toString() + " ";
                        }
                        outList.add(out);

                    } else {
                        String out="";
                       for(Object field:select.toList()){
                           out += (jsonObject.get((String) field)).toString() + " ";
                       }
                            outList.add(out);

                    }
                }

        );

        outList.forEach(System.out::println);

        return outList;
    }


    @Override
    public void addEmployee(String name, String address, int age) {

    }

    @Override
    public void addCar(String name, String address, int age) {

    }
}