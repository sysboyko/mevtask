package com.mev.task.DTO;

public class Condition {
   private String operator;
   private String val;
   private String field;

   public Condition(){}
    public Condition(String operator, String val, String field) {
        this.operator = operator;
        this.val = val;
        this.field = field;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
