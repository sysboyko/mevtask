package com.mev.task.controller;

import com.mev.task.service.SelectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @Autowired
    private SelectService selectService;

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/add")
    public String add(){
        return "addfields";
    }
    @RequestMapping("/all")
    public String all(){
        return "all";
    }

}
